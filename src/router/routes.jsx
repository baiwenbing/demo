import Login from "view/login";
import Home from 'view/home'
const Routes = [{
        path: "/login",
        component: Login
    },
    {
        path: "/home",
        component: Home
    }
]

export default Routes;