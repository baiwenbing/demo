import React from "react";
import { Route, BrowserRouter , Redirect } from "react-router-dom";

class RouterMap extends React.Component {
    render() {
        const { Routes } = this.props;
        const defaultRoute = <Route key={-1} exact path="/" component={()=>
            <Redirect to="/login"/>
        }/>;
        return <BrowserRouter>
            {
                Routes.length && Routes.map((itm,ind)=>{
                    return  <Route key={ind} path={itm.path} render={(api)=>{
                            return <itm.component key={ind} routes={itm.children} {...api}></itm.component>
                    }}></Route>       
                }).concat([defaultRoute])
            }
        </BrowserRouter >
    }
}
export default RouterMap;