import service from '../utils/request.js'

const get = (url, data) => {
  return service.get(url, {
    params: data
  });
}

const post = (url, data) => {
  return service.post(url, data);
}
// 1根据订单id获取订单信息
export function getOrderById() {
  return get('/api/banner?type=0')
}