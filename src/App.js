import React from 'react';
import './App.css'
import RouterView from './router'
   class App  extends React.Component {
      constructor(props){
        super(props)
      }
      render(){   
        return (
          <div className="App">
           <RouterView></RouterView>
          </div>
        );
      }
  
}


export default App;
