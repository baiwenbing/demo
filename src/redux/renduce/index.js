const defaultlist={
   list:[{
    id:1,
     name:"redux图片1",
   },
   {
    id:2,
    name:"redux图片2",
   }],
   diplist:[{
      id:1,
      img:'https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2534506313,1688529724&fm=26&gp=0.jpg'
   },
   {
    id:2,
     img:'https://dss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3984473917,238095211&fm=26&gp=0.jpg'
  }],
  tabname:'',
    text:{}
}

const markReducer=(state=defaultlist,action)=>{
  const {type,payload}=action;

  switch(type){
    case "TAB":
      return { ...state, tabname: payload };
      case "GETLIST":
        return { ...state, text: payload };
      default:
        return state;
  }

}
export default markReducer