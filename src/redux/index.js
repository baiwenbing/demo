import { createStore, combineReducers,applyMiddleware } from "redux";
import thunk from 'redux-thunk' // redux-thunk 支持 dispatch function，并且可以异步调用它
import logger from 'redux-logger' // 利用redux-logger打印日志
//把mark引进来
import markReducer from "./renduce/index.js";
const Renducers = combineReducers({
  markReducer
});
const Store = createStore( Renducers,applyMiddleware(thunk,logger) );

export default Store;